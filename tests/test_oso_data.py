#!/usr/bin/env python
"""Tests for `oso_data` package."""
# pylint: disable=redefined-outer-name
from datetime import datetime
from oso_data import __version__
from oso_data.oso_data_interface import OSO_DataOntologyInterface
from oso_data.oso_data_impl import OSO_DataOntology



def test_OSO_DataInterface():
    """ testing the formal interface (OSO_DataOntologyInterface)
    """
    assert issubclass(OSO_DataOntology, OSO_DataOntologyInterface)

def test_OSO_DataOntology():
   """ Testing OSO_DataOntology class"""
   osd = OSO_DataOntology()

   data1 = osd.osod_tbox.osod.OSO_Data("oso_data_11", 
                    hasRelDBID="324-342-234-234-234",
                    hasBarcode="123",
                    creator="Yvain",
                    wasCreatedOn=datetime.now(),
                    title="my first data",
                    description="this is my first data",
                    hasDatasetURL="http:://example.com/data_11") # hasDatasetURL=hasDatasetURL("http:://example.com/data_11")) #  hasDataSetURN=URN(urn="http:://example.com/data1"))

   assert data1.hasRelDBID == "324-342-234-234-234"
   assert data1.hasBarcode == "123"
   assert data1.creator == ["Yvain"]
   assert data1.title == ["my first data"]
   assert data1.description == "this is my first data"
   assert data1.hasDatasetURL == "http:://example.com/data_11"

   assert data1.wasCreatedOn < datetime.now()

    

