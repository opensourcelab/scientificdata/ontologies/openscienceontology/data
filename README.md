# OSO Data

Open Science Ontology for (Scientific) Data, based on EMMO (https://github.com/EMMO-repo)

## Features

## Installation

    pip install oso_data --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    oso_data --help 

## Development

    git clone gitlab.com/opensourcelab/data

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/data](https://opensourcelab.gitlab.io/data) or [data.gitlab.io](oso_data.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



