"""_____________________________________________________________________

:PROJECT: OSO Data Ontology

* Main module implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


import logging
import os
import pathlib
import logging
import pandas as pd
from datetime import datetime
from emmopy import get_emmo
from ontopy import World

from oso_data.emmo_utils import en, pl
from owlready2 import DatatypeProperty, FunctionalProperty, ObjectProperty, AllDisjoint

from oso_data.oso_data_interface import OSO_DataOntologyInterface
from oso_data.oso_data_tbox import OSODataTBox
from oso_data.export_ontology import export_ontology

class OSO_DataOntology(OSO_DataOntologyInterface):
    def __init__(self, 
                 emmo_filename : str = None,
                 oso_data_filename: str = None) -> None:
        """Implementation of the 
        """

        self.logger = logging.getLogger(__name__)

         # loading self.EMMO ontology
        self.emmo_world = World()

        if os.getenv("EMMO_FILENAME") is not None:
            emmo_filename = os.getenv("EMMO_FILENAME")
            
            print("==== emmo env set to: ", emmo_filename )

        if emmo_filename is not None:
            self.emmo = self.emmo_world.get_ontology(emmo_filename)    
        else:
            self.emmo = self.emmo_world.get_ontology()
        self.emmo.load()               # reload_if_newer = True
        self.emmo.sync_python_names()  # synchronize annotations
        #self.emmo.base_iri = self.emmo.base_iri.rstrip('/#')
        #self.catalog_mappings = {self.emmo.base_iri: self.emmo_url}

        self.osod_tbox = OSODataTBox(oso_data_tbox_filename=oso_data_filename, 
                                emmo_world=self.emmo_world,
                                emmo=self.emmo)

        self.logger.info("OSO_Data instance created")

    def export_ontologies(self, path: str = ".", format: str = 'turtle') -> None:
        """exporting all ontologies 

        :param path: path to the directory, where the ontologies should be stored
        :type path: str
        :param format: format of the ontologies, defaults to 'turtle'
        :type format: str, optional
        :raises NotImplementedError
        """
        self.logger.info("exporting ontologies")

        self.osod_tbox.export(path=path, format=format)
        
    
    def validate_ontologies(self) -> None:
        """validate all ontologies

        :raises NotImplementedError
        """
        raise NotImplementedError
    
        
    
