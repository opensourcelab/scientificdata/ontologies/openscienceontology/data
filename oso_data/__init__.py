"""Top-level package for OSO Data."""

__author__ = """mark doerr"""
__contributors__ = f"""{__author__}"""
__email__ = "mark@uni-greifswald.de"
__version__ = "0.0.2"
