"""_____________________________________________________________________

:PROJECT: OSO Data

* Main module formal interface. *

:details: In larger projects, formal interfaces are helping to define a trustable contract.
          Currently there are two commonly used approaches: 
          [ABCMetadata](https://docs.python.org/3/library/abc.html) or [Python Protocols](https://peps.python.org/pep-0544/)

       see also:
       ABC metaclass
         - https://realpython.com/python-interface/
         - https://dev.to/meseta/factories-abstract-base-classes-and-python-s-new-protocols-structural-subtyping-20bm

.. note:: -
.. todo:: - 
________________________________________________________________________
"""



from abc import ABCMeta, abstractclassmethod

class OSO_DataOntologyInterface(metaclass=ABCMeta):
    """ OSO_Data formal Interface
        TODO: test, if ABC baseclass is wor
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'export_ontologies') and 
                callable(subclass.export_ontologies) or 
                NotImplemented)
        
    @abstractclassmethod
    def __init__(self, emmo_filename: str = None,
                 oso_data_filename: str = None) -> None:
        raise NotImplementedError

    @abstractclassmethod 
    def export_ontologies(self, path: str = ".", format: str = 'turtle') -> None:
        """exporting all ontologies 

        :param path: path to the directory, where the ontologies should be stored
        :type path: str
        :param format: format of the ontologies, defaults to 'turtle'
        :type format: str, optional
        :raises NotImplementedError
        """
        raise NotImplementedError
    
    @abstractclassmethod
    def validate_ontologies(self) -> None:
        """validate all ontologies

        :raises NotImplementedError
        """
        raise NotImplementedError
    

